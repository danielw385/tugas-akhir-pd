package com.example.login;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

   private EditText Name;
   private EditText Password;
   private Button Login;
   private Button userWorker;
   private Button userCompany;
   private FirebaseAuth firebaseAuth;
   private ProgressDialog progressDialog;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        Name = (EditText)findViewById(R.id.etUserName);
        Password = (EditText)findViewById(R.id.etUserPassword);
        Login = (Button)findViewById(R.id.btnRegister);
        userWorker = (Button)findViewById(R.id.btnWorkerLogin);
        userCompany = (Button)findViewById(R.id.btnCompanyLogin);
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        FirebaseUser user = firebaseAuth.getCurrentUser();

        if(user != null) {
            startActivity(new Intent(MainActivity.this, Bottomnavigation.class));
            finish();
        }

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Name.getText().toString().isEmpty() && Password.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
                }
                else if(Password.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
                }
                else if(Name.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Please Enter All The Details", Toast.LENGTH_SHORT).show();
                }
                else {
                    validate(Name.getText().toString(), Password.getText().toString());
                }
            }
        });

        userWorker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegistrationActivity.class));
            }
        });

        userCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, registrationcompany.class));
            }
        });

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            if(bundle.getString("some")!= null){
                Toast.makeText(getApplicationContext(), "data:" + bundle.getString("some" ), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void validate(String userName, String userPassword) {

        progressDialog.setMessage("Loading... Please Wait");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(userName, userPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Welcome To JobDesk", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, Bottomnavigation.class));
                    finish();
                }
                else {
                    Toast.makeText(MainActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        });
    }

    private Boolean validate(){
        Boolean result = false;

        String first = Name.getText().toString();
        String last = Password.getText().toString();

        if(first.isEmpty() && last.isEmpty()){
            Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
        }
        else{
            result = true;
        }

        return result;
    }
}
